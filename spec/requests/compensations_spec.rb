require 'rails_helper'

RSpec.describe 'Compensations', type: :request do
  let(:customer) { create(:customer) }
  let!(:lawyer) { create(:lawyer, state: customer.state) }
  let(:right_params) {
    {
      'compensation': {
        'title': 'Meu voo foi cancelado, como proceder?'
      }
    }
  }

  describe 'POST /customer/:id/compensations' do
    context 'when user is logged' do
      before(:each) do
        sign_in customer
        post customer_compensations_path(customer.id), params: right_params
      end

      it 'redirects to customer path' do
        expect(response).to redirect_to customer_path(customer.id)
      end

      it 'creates a new compensation requirement' do
        compensation_count = Compensation.count
        expect(compensation_count).to eq(1)
      end
    end

    context 'when user is not logged' do
      before(:each) do
        post customer_compensations_path(customer.id), params: right_params
      end

      it 'redirects to login' do
        expect(response).to redirect_to login_path
      end

      it 'does not create a new compensation requirement' do
        compensation_count = Compensation.count
        expect(compensation_count).to eq(0)
      end
    end
  end

  describe 'DELETE /customer/:id/compensations' do
    let(:compensation) { create(:compensation, customer: customer, lawyer: lawyer) }

    context 'when user is logged' do
      before(:each) do
        sign_in customer
        delete customer_compensation_path(customer.id, compensation.id)
      end

      it 'redirects to customer path' do
        expect(response).to redirect_to customer_path(customer.id)
      end

      it 'creates a new compensation requirement' do
        compensation_count = Compensation.count
        expect(compensation_count).to eq(0)
      end
    end

    context 'when user is not logged' do
      before(:each) do
        delete customer_compensation_path(customer.id, compensation.id)
      end

      it 'redirects to login' do
        expect(response).to redirect_to login_path
      end

      it 'does not create a new compensation requirement' do
        compensation_count = Compensation.count
        expect(compensation_count).to eq(1)
      end
    end
  end

  def sign_in(customer)
    post sessions_path, params: { user: { email: customer.email, password: 'test' } }
  end
end
