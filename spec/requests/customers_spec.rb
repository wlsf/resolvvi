require 'rails_helper'

RSpec.describe 'Customers', type: :request do
  let(:state) { create(:state) }
  let(:wrong_params) {
    {
      'customer': {
        'name': 'Jose',
        'email': '',
        'password': '',
        'password_confirmation': ''
      }
    }
  }
  let(:right_params) {
    {
      'customer': {
        'name': 'Jose Valim',
        'email': 'jose.valim@elixir.com',
        'password': 'test',
        'password_confirmation': 'test',
        'state_id': state.id
      }
    }
  }

  describe 'POST /customers (#signup)' do
    context 'when params are not valid' do
      before(:each) do
        post customers_path, params: wrong_params
      end

      it 'redirects to signup' do
        expect(response).to redirect_to signup_path
      end

      it 'does not creates a new customer' do
        customer_count = Customer.count
        expect(customer_count).to eq(0)
      end
    end

    context 'when params are valid' do
      before(:each) do 
        post customers_path, params: right_params 
      end

      it 'returns 201 :created' do
        expect(response).to have_http_status :created
      end

      it 'does not creates a new customer' do
        customer_count = Customer.count
        expect(customer_count).to eq(1)
      end
    end
  end

  describe 'GET /customer' do
    context 'when user is logged' do
      let(:customer) { create(:customer) }
      before(:each) do
        sign_in customer
        get customer_path(customer.id)
      end

      it 'returns 200 :ok' do
        expect(response).to have_http_status :ok
      end
    end

    context 'when user is not logged' do
      let(:customer) { create(:customer) }
      before(:each) do
        get customer_path(customer.id)
      end

      it 'redirects to login' do
        expect(response).to redirect_to login_path
      end
    end
  end

  def sign_in(customer)
    post sessions_path, params: { user: { email: customer.email, password: 'test' } }
  end
end
