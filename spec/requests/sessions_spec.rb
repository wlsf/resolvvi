require 'rails_helper'

RSpec.describe 'Sessions', type: :request do
  let(:wrong_params) {
    {
      'user': {
        'email': 'test@gmail.com',
        'password': 'test'
      }
    }
  }
  let(:customer) { create(:customer, password: 'test') }
  let(:right_params) {
    {
      'user': {
        'email': customer.email,
        'password': 'test'
      }
    }
  }

  describe 'POST /sessions (#login)' do
    context 'when user doesnt exist' do
      it 'redirects to login' do
        post sessions_path, params: wrong_params
        expect(response).to redirect_to login_path
      end
    end

    context 'when user exists' do
      before(:each) do
        post sessions_path, params: right_params
      end

      it 'redirects to user dashboard' do
        expect(response).to redirect_to customer_path(customer.id)
      end

      it 'starts a new session with user_id' do
        expect(session[:user_id]).to eq(customer.id)
      end
    end
  end

  describe 'DELETE /sessions (#logout)' do
    context 'when user is logged' do
      before(:each) do
        get logout_path, params: right_params
      end

      it 'redirects to login' do
        expect(response).to redirect_to login_path
      end

      it 'destroys the user session' do
        expect(session[:user_id]).to be_nil
      end
    end
  end
end
