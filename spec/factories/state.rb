FactoryBot.define do
  factory :state, class: 'State' do
    name { Faker::Address.state }
  end
end