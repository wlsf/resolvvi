FactoryBot.define do
  factory :customer, class: 'Customer' do
    name { Faker::Name.name }
    email { Faker::Internet.email }
    association :state, factory: :state
    password { 'test' }
  end
end