FactoryBot.define do
  factory :lawyer, class: 'Lawyer' do
    name { Faker::Name.name }
    email { Faker::Internet.email }
    association :state, factory: :state
    password { 'test' }
  end
end