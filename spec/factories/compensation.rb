FactoryBot.define do
  factory :compensation, class: 'Compensation' do
    title { Faker::Lorem.sentence }
    association :customer, factory: :customer
    association :lawyer, factory: :lawyer
  end
end