require 'rails_helper'

RSpec.describe Lawyer, type: :model do
  describe 'associations' do
    it { should have_many(:compensations) } 
    it { should have_many(:clients).through(:compensations) } 
  end
end
