# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

states = State.create([
  { name: 'São Paulo' },
  { name: 'Fortaleza' },
  { name: 'Rio de Janeiro' },
  { name: 'Mato Grosso' },
  { name: 'Mato Grosso do Sul' }
])

Lawyer.create({ name: 'James Rodrigues', email: 'james@rodrigues.com', password: 'passwordtest', state: states[0] })
Lawyer.create({ name: 'Alessandro Alves', email: 'alessandro@alves.com', password: 'passwordtest', state: states[1] })
Lawyer.create({ name: 'Bruna Jones', email: 'bruna@jones.com', password: 'passwordtest', state: states[2] })
Lawyer.create({ name: 'Philip DeLonge', email: 'philip@delonge.com', password: 'passwordtest', state: states[3] })
Lawyer.create({ name: 'John Doe', email: 'john@doe.com', password: 'passwordtest', state: states[4] })

Lawyer.create({ name: 'Alan Carlito', email: 'alan@carlito.com', password: 'passwordtest', state: states[0] })
Lawyer.create({ name: 'Adrian Baptist', email: 'adrian@baptist.com', password: 'passwordtest', state: states[1] })
Lawyer.create({ name: 'Aric Madson', email: 'aric@madson.com', password: 'passwordtest', state: states[2] })
Lawyer.create({ name: 'Ariel Jesus', email: 'ariel@jesus.com', password: 'passwordtest', state: states[3] })
Lawyer.create({ name: 'Tony Levis', email: 'tony@levis.com', password: 'passwordtest', state: states[4] })