class CreateCompensations < ActiveRecord::Migration[5.2]
  def change
    create_table :compensations do |t|
      t.references :customer, index: true, foreign_key: { to_table: :users }
      t.references :lawyer, index: true, foreign_key: { to_table: :users }
      t.string :title

      t.timestamps
    end
  end
end
