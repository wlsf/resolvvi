Rails.application.routes.draw do
  resources :customers, only: [:new, :create, :show] do
    resources :compensations, only: [:create, :destroy]
  end
  resources :sessions, only: [:new, :create, :destroy]

  get 'signup', to: 'customers#new', as: :signup
  get 'login', to: 'sessions#new', as: :login
  get 'logout', to: 'sessions#destroy', as: :logout

  root to: 'sessions#new'
end
