# Setting up the project

Ruby -v 2.6.3
Rails -v 5.2.3
Gem -v 3.0.3
Bundle -v 2.0.2

```
bundle install
rails db:migrate
rails db:seed

rails s
```

## Como você abordou o desafio?
## Qual schema design você escolheu e por que?

Tentei focar na usabilidade, entre telas, quantidade de clicks, facilidade para acessar e visualizar informação, etc.
- Foi adicionado algumas seeds para preencher o banco com Estados e Advogados padrões para atender os devidos casos.
- Pra cada pedido de indenização criado pela primeira vez, um Advogado do mesmo Estado do Cliente é selecionado automagicamente.
- Após o primeiro pedido criado por determinado Cliente, os próximos serão atendidos pelo mesmo Advogado.
- Como não foi estabelecido uma diferença entre cliente e advogado na descrição do teste, optei por criar uma tabela só e utilizar esquema de herança do próprio ActiveRecord com a coluna `type`
- Não existe uma tabela pra relacionar Advogado diretamente com Cliente, optei por deixar isso em anexo com o pedido de indenização.
- Optei por utilizar tudo que a Framework do Rails disponibiliza para o desenvolvimento de uma aplicação Full Stack, isso facilita e agiliza o desenvolvimento de um MVP deste tipo.
- Optei por não seguir a risca alguns padrões de CRUD que o DHH prega no uso de controllers, achei que isso seria um pouco prejudicial para o cliente final se por acaso ele tivesse que frequentar várias telas diferentes para conseguir fazer um fluxo como por ex:
fazer registro -> ir para tela de criação de pedidos -> definir o pedido -> ver a listagem de pedidos -> ir para tela de perfil
ao invés disso os fluxos ficaram mais enxutos também:
fazer registro -> tela de dashboard (listagem de pedidos, navbar, nome do usuário e possíveis novas informações)

Com isso a deleção e criação de novos pedidos pode ser feito através desse dashboard seguindo um simples modal.



## Se você tivesse mais um dia para trabalhar no desafio, como o usaria? E se você tivesse um mês?
Se tivesse mais 1 dia, eu focaria em deixar o MVP mais robusto.
Adicionaria popups e notifiers para conseguir fazer a aplicação se comunicar melhor com o usuário, atualmente está faltando feedbacks para o usuário das ações que ele toma.

Considerando que o sistema completo deve atender usuários, advogados, e outros tipos de rotação de informações (até mesmo admin), eu adicionaria um sistema de login mais eficaz e com políticas de acessos (ex: Devise, CanCan), em consequência isso melhoraria a segurança também.

Além de focar mais no layout da aplicação, conseguir modularizar melhor os estilos adicionados, manipular eles de forma que o site/MVP ganhe um brand mais aceitável(cool) e consistente.


Se tivesse mais 1 mês, seria possível focar na estruturação mais paupável da aplicação a nível de Engenharia, separando melhor os conceitos e responsabilidades dentro do app, melhorando a classificação de match entre advogados e clientes, adicionando novas camadas de testes para o front e incrementando as camadas já existentes, tentando capturar edge cases mais complicados.

Além disso, conseguiria também criar um ambiente de desenvolvimento com uma cara mais plug-n-play, utilizando Docker para containerizar ferramentas externas como o próprio Postgres por exemplo. Adicionaria uma camada de verificação de qualidade de código a partir de linters/analyzers para garantir que o processo de desenvolvimento pudesse ser feito mantendo a consistência das decisões de engenharia.

Todos esses pontos preparariam o MVP para receber um CI/CD adequado para quando fosse decidido investir mais pesado na ideia.