class Lawyer < User
  has_many :compensations
  has_many :clients, through: :compensations, source: :customer
end