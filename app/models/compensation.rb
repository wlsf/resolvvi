class Compensation < ApplicationRecord
  include LawyersHelper

  belongs_to :customer
  belongs_to :lawyer

  validates_presence_of :title
  validates_presence_of :customer, :lawyer

  before_validation(on: :create) do
    self.lawyer = match_lawyer(self.customer)
  end
end
