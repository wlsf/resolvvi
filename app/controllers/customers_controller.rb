class CustomersController < ApplicationController
  before_action :require_login, only: [:show]
  before_action :set_customer, only: [:show]

  def new
    @customer = Customer.new
  end

  def create
    @customer = Customer.new(customer_params)

    if @customer.save
      session[:user_id] = @customer.id
      render :show, status: :created, location: @customer
    else
      redirect_to signup_path
    end
  end

  def show
  end

  private

  def set_customer
    @customer = Customer.find_by(id: params[:id])
  end

  def customer_params
    params.require(:customer).permit(:name, :password, :password_confirmation, :email, :state_id)
  end
end