class ApplicationController < ActionController::Base

  def require_login
    unless valid_session?
      redirect_to login_path
    else
      redirect_to customer_path(user) unless user_can?
      user
    end
  end

  def dispatch_logged_users
    if valid_session?
      redirect_to customer_path(user)
    end
  end

  def user
    get_logged_user
  end

  private

  def user_can?
    user_id = get_logged_user.id
    request&.params[:customer_id]&.to_i == user_id || request&.params[:id]&.to_i == user_id
  end

  def get_logged_user
    @current_customer ||= Customer.find_by(id: session_user_id)
  end

  def valid_session?
    session_user_id.present?
  end

  def session_user_id
    session[:user_id]
  end
end
