class CompensationsController < ApplicationController
  before_action :require_login
  before_action :set_customer
  before_action :set_compensation, only: :destroy

  def create
    @compensation = @customer.compensations.new(compensation_params)
  
    if @compensation.save!
      redirect_to customer_path(@customer.id)
    else
      redirect_to customer_path(@customer.id)
    end
  end

  def destroy
    @compensation.destroy   
    redirect_to customer_path(@customer.id)
  end

  private

  def set_customer
    @customer = Customer.find_by(id: params[:customer_id])
  end

  def set_compensation
    @compensation = @customer.compensations.find_by(id: params[:id])
  end

  def compensation_params
    params.require(:compensation).permit(:title)
  end
end
