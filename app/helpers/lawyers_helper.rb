module LawyersHelper
  def match_lawyer(customer)
    return current_lawyer(customer) if has_lawyer?(customer)

    ids = Lawyer.where(state: customer.state).pluck(:id)

    Lawyer.find_by(id: ids.sample)
  end

  private

  def current_lawyer(customer)
    customer.compensations&.first&.lawyer
  end

  def has_lawyer?(customer)
    customer.compensations.count > 0
  end
end